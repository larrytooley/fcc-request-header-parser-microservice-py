# Free Code Camp Response Header Parser Microservice in Python

Inspired by [this][1] Free Code Camp challenge. You can find the Node.js version of my solution [here][2]

App running on Heroku [here][3].

[1]: https://www.freecodecamp.com/challenges/timestamp-microservice
[2]: https://github.com/larrytooley/fcc-request-header-parser-microservice
[3]: https://damp-oasis-21715.herokuapp.com/
