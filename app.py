from flask import Flask
from flask import request
import os
import json

app = Flask(__name__)
port = int(os.environ.get('PORT', 5000))

@app.route("/")
def api():
    """
    Return ip, langauge, and user agent as json
    """
    header_dict = dict(request.headers)

    ip = request.remote_addr 
    language = header_dict["Accept-Language"].split(',')[0]
    ua = header_dict["User-Agent"].split('(')[1].split(')')[0]


    data = {
        "ipaddress": ip,
        "language": language,
        "software": ua}

    data_json = json.JSONEncoder().encode(data)

    return data_json

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)
